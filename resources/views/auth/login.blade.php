@extends('auth.layout')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <h1>Авторизация</h1>
            </div>

            @error('login')
            <div class="alert alert-danger">
                {{ $errors->first('login') }}
            </div>
            @enderror

            <form action="{{ route('login.doLogin') }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="l_email">Email</label>
                    <input id="l_email" class="form-control" type="email" name="email" required>
                </div>

                <div class="form-group">
                    <label for="l_password">Пароль</label>
                    <input id="l_password" class="form-control" type="password" name="password" required>
                </div>

                <div class="form-group">
                    <label>
                        <input type="checkbox" name="remember">
                        Запомнить меня
                    </label>
                </div>

                <button class="btn btn-primary">Войти</button>
                <a class="btn btn-light" href="{{ route('registration.index') }}">
                    Регистрация
                </a>

            </form>
        </div>
    </div>
@endsection
