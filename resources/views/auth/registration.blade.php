@extends('auth.layout')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <h1>Регистрация</h1>
            </div>

            <form action="{{ route('registration.doRegister') }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="r_email">Email</label>
                    <input id="r_email" class="form-control is-invalid" type="text" name="email">
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="r_password">Пароль</label>
                    <input id="r_password" class="form-control" type="password" name="password">
                </div>

                <div class="form-group">
                    <label for="r_password_c">Подтвердите пароль</label>
                    <input id="r_password_c" class="form-control" type="password" name="password_confirmation">
                </div>

                <div class="form-group">
                    <label for="r_fname">Имя</label>
                    <input id="r_fname" class="form-control" type="text" name="first_name">
                </div>

                <div class="form-group">
                    <label for="r_lname">Фамилия</label>
                    <input id="r_lname" class="form-control" type="text" name="last_name">
                </div>


                <button type="submit" class="btn btn-primary">Регистрация</button>

            </form>
        </div>
    </div>

    @foreach($errors->all() as $error)
        <pre>
            <b>
                {{ $error }}
            </b>
        </pre>
    @endforeach


@endsection
