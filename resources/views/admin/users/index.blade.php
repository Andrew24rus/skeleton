@extends('admin.layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>role</th>
            <th>email</th>
            <th>first name</th>
            <th>last name</th>
            <th>last login</th>
            <th>registered at</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
        <tr>
            <td></td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->last_login }}</td>
            <td>{{ $user->created_at }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
