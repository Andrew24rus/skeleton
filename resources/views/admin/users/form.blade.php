@extends('admin.layout')

@section('content')
    <form action="{{ route('users.update', ['user' => $user->id]) }}" method="post">
        @if($user)
            @method('put')
        @endif

        @csrf

        <div class="form-group">
            <label for="">Имя</label>
            <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
        </div>

        <div class="form-group">
            <label for="">Фамилия</label>
            <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
@endsection
