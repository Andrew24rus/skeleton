<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Skeleton</title>

    <link rel="stylesheet" href="{{ mix('css/style.css') }}">

    @yield('style')
</head>
<body>

    <div class="container-fluid">
        <header class="row mb-5 mt-3">

            <div class="col-12">
                <a href="">{{ $user->email }}</a>

                <a href="{{ route('login.doLogout') }}" class="btn btn-danger">Выход</a>
            </div>
        </header>

        <main>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield('content')
        </main>

        <footer></footer>
    </div>


@yield('script')

</body>
</html>
