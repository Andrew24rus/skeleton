<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Sentinel Routes
|--------------------------------------------------------------------------
*/

Route::name('login.')->group(function() {
    Route::get('/login', [AuthController::class, 'index'])->name('index');
    Route::post('/login', [AuthController::class, 'login'])->name('doLogin');
    Route::get('/logout', [AuthController::class, 'logout'])->name('doLogout');
});

Route::name('registration.')->group(function() {
    Route::get('/registration', [RegisterController::class, 'index'])->name('index');
    Route::post('/registration', [RegisterController::class, 'register'])->name('doRegister');
});
