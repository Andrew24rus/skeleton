<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Здесь описаны административные маршруты
| Для доступа требуется авторизация пользователя
|
*/

Route::resource('users', UserController::class);
