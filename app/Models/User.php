<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function($user) {
            Validator::make($user->toArray(), [
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
            ])->validate();
        });

        static::saving(function($user) {
            Validator::make($user->toArray(), [
                'first_name' => 'max:255',
                'last_name' => 'max:255'
            ])->validate();
        });
    }


}
