<?php

namespace App\Http\Controllers;

use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Show users list.
     */
    public function index(Request $request)
    {
        $users = User::orderBy('id', 'desc')->paginate(25);

        return view('admin.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new user.
     */
    public function create(Request $request)
    {
        return view('admin.users.form');
    }

    /**
     * Store a newly created user in storage.
     */
    public function store(Request $request)
    {
        ddd('store');
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.form', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        ddd('edit');
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }
}
