<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $user = Sentinel::forceAuthenticate($credentials, $request->has('remember'));

        if (!$user) {
            return back()->withErrors(['login' => 'Неверно введен логин или пароль']);
        }

        return redirect('/');

    }

    public function logout()
    {
        Sentinel::logout();

        return redirect('/');
    }
}
