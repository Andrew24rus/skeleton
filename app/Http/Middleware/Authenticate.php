<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;

class Authenticate
{
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()) {
            return $next($request);
        }

        return redirect($this->redirectTo($request));
    }


    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login.index');
        }
    }
}
